# Class

## Fields:

### attribute
Base attributes given by this class.
### armor_type
Maximum armor class that can be worn by this class.
### dmg_attr
Attribute used for damage calculation.
### weapon_types
Weapons this class can use.
### skill_tree
Skilltree of this class