# Inventory

## Fields:

### owner
Reference to the character this inventory belongs to.
### items
Vector of items in the inventory.
### mass
Collective amount of mass of all items in this inventory.

## Functions:

### max_mass
Return the maximum amount of weight the inventory can hold.
### equip_inv_item
TODO
### put
Inserts item into the inventory.
### pull
Returns an item at the given index.