#ifndef CPPRPG_CHARACTERS_EQUIPMENT
#define CPPRPG_CHARACTERS_EQUIPMENT

#include <array>
#include <cpprpg/common/config.hpp>
#include <cpprpg/items/item.hpp>
#include <functional>
#include <memory>
#include <optional>

class Equipment
{
	using equip_tuple = std::tuple<std::optional<Item>, attr_type, bool>;

	private:
	Character& owner;
	/// the pair is the equipment and its deductions
	std::array<equip_tuple, 10> arr;
	/// tests the equipment for fulfilled requirements, returns false if any test failed
	bool test_all_equips() const;
	/// equips the item without tests;
	void just_equip(Item wearable, SlotType slot);

	public:
	/// first unequips the old item, then tests if the new item can still be equipped, if it can, it
	/// returns {true, old_item} otherwise {false, nullptr}. old_item can be a nullptr if it was
	/// empty
	std::pair<bool, std::array<std::optional<Item>, 2>> equip(Item wearable, SlotType slot);
	/// returns the item in slot
	std::optional<Item> unequip(SlotType slot);
	/// calculates the deductions
	// TODO: don't take old deductions into account
	void evaluate();
	Equipment(Character& owner);
	/// gathers all attributes from the current equipment and subtracts the deductions (if an
	/// attribute is negative, it is increases instead of lowered
	Attributes get_attributes()
	{
		Attributes reattr;
		for (auto& tuple : arr)
		{

			if(std::get<std::optional<Item>>(tuple) && std::get<bool>(tuple))
			{
				attr_type factor = std::get<attr_type>(tuple) / attr_type(100);
				Attributes tmp = std::get<std::optional<Item>>(tuple)->attr;

				for (auto& attr : tmp.arr)
				{
					attr = attr >= 0 ? attr * factor : attr * (1 - factor + 1);
				}
				reattr += tmp;
			}
		}
		return reattr;
	}

	std::optional<Item>& get_equip(SlotType slot)
	{
		return std::get<std::optional<Item>>(get_tuple(slot));
	}

	/// returns a reference to the item in slot
	equip_tuple& get_tuple(SlotType slot)
	{
		switch (slot)
		{
		case SlotType::BACK:
			return arr[0];
		case SlotType::NECK:
			return arr[1];
		case SlotType::SHOULDERS:
			return arr[2];
		case SlotType::CHEST:
			return arr[3];
		case SlotType::HEAD:
			return arr[4];
		case SlotType::LEGS:
			return arr[5];
		case SlotType::FEET:
			return arr[6];
		case SlotType::FINGER:
			return arr[7];
		case SlotType::MAINH:
			return arr[8];
		case SlotType::SIDEH:
			return arr[9];
		}
		throw std::logic_error("Out of Enum");
	}
};
#endif // CPPRPG_CHARACTERS_EQUIPMENT
