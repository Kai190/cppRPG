#ifndef CPPRPG_CHARACTERS_INVENTORY
#define CPPRPG_CHARACTERS_INVENTORY

#include <cpprpg/items/item.hpp>
#include <memory>
#include <vector>

class Character;

class Inventory
{
	private:
	Character& owner;

	public:
	unsigned max_mass();
	std::vector<Item> items;
	unsigned mass = 0;
	Inventory(Character& owner) : owner(owner) {}
	/// tries to equip the item in index into slot of the owner
	bool equip_inv_item(size_t index, SlotType slot);
	/// puts i into inventory, if successful, i is set to nullptr
	std::optional<Item> put(Item i)
	{
		if(mass + i.mass < max_mass())
		{
			mass += i.mass;
			items.push_back(std::move(i));
			return std::nullopt;
		}
		return i;
	}
	/// returns the item at index
	Item pull(size_t index)
	{
		Item tmp = std::move(items[index]);
		items.erase(items.begin() + index);
		mass -= tmp.mass;
		return tmp;
	}
};
#endif // CPPRPG_CHARACTERS_INVENTORY
