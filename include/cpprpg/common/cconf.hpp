#ifndef CPPRPG_COMMON_CCONF_HPP2
#define CPPRPG_COMMON_CCONF_HPP2

#include <variant>
#include <string>
#include <map>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>

std::tuple<size_t,size_t> get_textpos(std::ifstream& file);


using cconf_int = long;
using cconf_float = float;
using cconf_str = std::string;

using cconf_value = std::variant<std::string, cconf_int, cconf_float, bool>;
using cconf_kv = std::map<std::string, std::variant<cconf_value, std::vector<cconf_value>>>;
using cconf_section = std::map<std::string, cconf_kv>;
using cconf_vector_variant = std::variant<std::vector<std::string>,
                                          std::vector<cconf_int>,
                                          std::vector<cconf_float>,
                                          std::vector<bool>>;

enum class cconf_token{SECTION, KEY, VALUE};
enum class cconf_type{STRING, INT, FLOAT, BOOL, ARRAY, NONE};

using cconf_tokens =
    std::vector<std::tuple<cconf_token, std::variant<std::string, std::vector<std::string>>>>;

template <typename T>
T cconf_get_value(const std::variant<cconf_value, std::vector<cconf_value>>& val)
{
	return std::get<T>(std::get<cconf_value>(val));
}

template <>
inline std::vector<cconf_value> cconf_get_value<std::vector<cconf_value>>(const std::variant<cconf_value, std::vector<cconf_value>>& val)
{
	return std::get<std::vector<cconf_value>>(val);
}

class cconf
{
	int skip_whitespace(std::ifstream& file, bool until_newline)
	{
		int c;
		while ((c = file.get()) != EOF && isspace(c) && (!until_newline || c != '\n'))
			;

		if (c != EOF)
			file.unget();

		return c;
	}

	void string_toend(std::ifstream& file, std::string& str)
	{
		int c;
		if ((c = file.get()) != '"')
			throw std::logic_error("string didn't start with '\"'");

		while ((c = file.get()) != EOF)
		{
			if (c == '"')
			{
				return;
			}
			else if (c == '\\')
			{
				escaped_toend(file, str);
			}
			else
			{
				str += c;
			}
		}
		throw std::runtime_error(name + ": EOF encountered while parsing string");
	}

	void escaped_toend(std::ifstream& file, std::string& str)
	{
		if (int c = file.get(); c == 'n')
		{
			str += '\n';
		}
		else if (c == '"')
		{
			str += '"';
		}
		else if (c == 't')
		{
			str += '\t';
		}
		else if (c == '\\')
		{
			str += '\\';
		}
		else
		{
			auto [line, lineoffset] = get_textpos(file);
			std::stringstream ss;
			ss << name << ":" << line << ":" << lineoffset - 1
			   << " Error while trying to parse escaped character";
			throw std::runtime_error(ss.str());
		}
	}

	void string_toend(const std::string& from, std::string& to)
	{
		int c;
		size_t at = 0;
		if ((c = from[at++]) != '"')
			throw std::logic_error("string didn't start with '\"'");

		while (at < from.size())
		{
			c = from[at++];
			if (c == '"')
			{
				return;
			}
			else if (c == '\\')
			{
				escaped_toend(from, at, to);
			} else {
				to += c;
			}
		}
		throw std::runtime_error(name + ": EOF encountered while parsing string");
	}

	void escaped_toend(const std::string& from, size_t& start, std::string& to)
	{
		if (int c = from[start++]; c == 'n')
		{
			to += '\n';
		}
		else if (c == '"')
		{
			to += '"';
		}
		else if (c == 't')
		{
			to += '\t';
		}
		else if (c == '\\')
		{
			to += '\\';
		}
		else
		{
			throw std::runtime_error(
			    name + ": Error while trying to parse escaped character in value string");
		}
	}

	void section_toend(std::ifstream& file, std::string& name)
	{
		int c;
		if ((c = file.get()) != '[')
			throw std::logic_error("section didn't start with \"[\"");

		while ((c = file.get()) != EOF)
		{
			if (c == ']')
			{
				return;
			}
			else if (c == '\n')
			{
				auto [line, lineoffset] = get_textpos(file);
				std::stringstream ss;
				ss << name << ":" << line << ":" << lineoffset - 1
				   << " Newline encountered while parsing section name";
				throw std::runtime_error(ss.str());
			}
			else
			{
				name += c;
			}
		}
		throw std::runtime_error(name + ": EOF encountered while parsing section name");
	}

	void key_toend(std::ifstream& file, std::string& key)
	{
		int c = file.peek();
		if (c == EOF)
			throw std::logic_error(name + ": EOF at start of key");

		if (c == '"')
		{
			string_toend(file, key);
		}
		else
		{
			while ((c = file.get()) != EOF && !isspace(c) && c != '=')
			{
				key += c;
			}
			if (c == '=')
				file.unget();
		}
		c = skip_whitespace(file, false);

		if (c == EOF)
		{
			throw std::runtime_error(name + ": EOF encountered while parsing key");
		}
		else if (c != '=')
		{
			auto [line, lineoffset] = get_textpos(file);
			std::stringstream ss;
			ss << name << ":" << line << ":" << lineoffset << " Expected \"=\", but got '"
			   << (char)c << "'";
			throw std::runtime_error(ss.str());
		}
		else
		{
			file.get(); // skip '='
		}
	}

	void value_toend(std::ifstream& file,
	                 std::variant<std::string, std::vector<std::string>>& value)
	{
		int c = skip_whitespace(file, false);

		if (c == EOF)
		{
			throw std::runtime_error(name + ": EOF encountered while parsing value");
		}
		else if (c == '"')
		{
			std::string tmp;
			string_toend(file, tmp);
			value = tmp;
		}
		else if (c == '[')
		{
			std::vector<std::string> tmp;
			array_toend(file, tmp);
			value = tmp;
		}
		else
		{
			std::string tmp;
			while ((c = file.get()) != EOF && !isspace(c))
			{
				tmp += c;
			}
			value = std::move(tmp);
			if (isspace(c))
				file.unget();
		}

		c = skip_whitespace(file, true);

		if (!(c == '\n' || c == EOF))
		{
			auto [line, lineoffset] = get_textpos(file);
			std::stringstream ss;
			ss << name << ":" << line << ":" << lineoffset
			   << " Key started on same line as last key";
			throw std::runtime_error(ss.str());
		}
	}

	void array_toend(std::ifstream& file, std::vector<std::string>& arr)
	{
		arr.emplace_back();
		int c;
		if ((c = file.get()) != '[')
			std::logic_error("array didn't start with '['");

		while ((c = file.get()) != EOF)
		{

			bool set = false;
			if (c == ']')
			{
				return;
			}
			else if (c == ',')
			{
				arr.emplace_back();
				set = false;
			} else if(c == '"') {
				if (set)
					throw std::runtime_error(name + ": Expected ',', but got '\"'");
				file.unget();
				string_toend(file, arr.back());
				set = true;
			} else {
				if (set)
					throw std::runtime_error(name + ": Expected ',', but got " + (char)c);
				file.unget();
				while ((c = file.get()) != EOF && !isspace(c) && c != ',' && c != ']')
					arr.back() += c;
				if (c != EOF)
					file.unget();
				set = true;
			}
			skip_whitespace(file, false);
		}
		throw std::runtime_error(name + ": EOF encountered while parsing array");
	}

	cconf_tokens tokenize(std::ifstream& file)
	{
		cconf_tokens ret;

		int c;
		while (true)
		{
			c = skip_whitespace(file, false);
			if (c == '[')
			{
				std::string sectionname;
				section_toend(file, sectionname);
				ret.push_back({cconf_token::SECTION, sectionname});
			}
			else if (c == EOF)
			{
				break;
			}
			else
			{
				std::string key;
				std::variant<std::string, std::vector<std::string>> value;
				key_toend(file, key);
				value_toend(file, value);
				ret.push_back({cconf_token::KEY, key});
				ret.push_back({cconf_token::VALUE, value});
			}
		}

		return ret;
	}

	void print_tokens(cconf_tokens tokens)
	{
		for (auto [token_type, token] : tokens)
		{
			if (token_type == cconf_token::SECTION)
				std::cout << "SECTION";
			else if (token_type == cconf_token::KEY)
				std::cout << "KEY";
			else if (token_type == cconf_token::VALUE)
				std::cout << "VALUE";

			if (std::holds_alternative<std::string>(token))
				std::cout << " \"" << std::get<std::string>(token) << "\"" << std::endl;
			else
			{
				std::cout << "[";
				for (auto item : std::get<std::vector<std::string>>(token))
				{
					std::cout << "\"" << item << "\""
					          << ",";
				}
				std::cout << "]" << std::endl;
			}
		}
	}

	void parse_tokens(cconf_tokens tokens)
	{
		cconf_kv* kv = &sections[""];
		std::variant<cconf_value, std::vector<cconf_value>>* value = nullptr;
		for (auto token : tokens)
		{
			if (std::get<0>(token) == cconf_token::SECTION)
			{
				kv = &sections[std::get<std::string>(std::get<1>(token))];
			}
			else if (std::get<0>(token) == cconf_token::KEY)
			{
				value = &(*kv)[std::get<std::string>(std::get<1>(token))];
			}
			else if (std::get<0>(token) == cconf_token::VALUE)
			{
				*value = parse_value(std::get<1>(token));
			} else {
				throw std::logic_error("unknown token");
			}
		}
	}

	cconf_value parse_scalar(std::string val_str, cconf_type& type)
	{
		cconf_value val;
		try // int
		{
			type = cconf_type::INT;
			size_t i;
			val = (cconf_int)std::stoi(val_str, &i, 0);
			if (i != val_str.size())
				throw std::exception();
			else
				return val;
		}
		catch (...)
		{
		}

		try // float
		{
			type = cconf_type::FLOAT;
			size_t i;
			val = (cconf_float)std::stof(val_str, &i);
			if (i != val_str.size())
				throw std::exception();
			else
				return val;
		}
		catch (...)
		{
		}

		if (val_str.compare("true") == 0)
		{
			type = cconf_type::BOOL;
			return true;
		}
		else if (val_str.compare("false") == 0)
		{
			type = cconf_type::BOOL;
			return false;
		}
		type = cconf_type::STRING;
		if (val_str[0] == '\"')
		{
			std::string str;
			string_toend(val_str, str);
			return str;
		}
		else
		{
			std::string str;
			int c;
			size_t at = 0;
			while (at != val_str.size())
			{
				c = val_str[at++];
				str += c;
			}
			return str;
		}
	}

	std::vector<cconf_value> parse_array(std::vector<std::string> values)
	{
		std::vector<cconf_value> ret;

		cconf_type lasttype = cconf_type::NONE;

		for (auto val : values)
		{
			cconf_type type;
			auto tmp = parse_scalar(val, type);
			if (lasttype == cconf_type::NONE || lasttype == type)
			{
				lasttype = type;
				ret.push_back(tmp);
			}
			else
			{
				throw std::runtime_error(name + ": Mixed types in array");
			}
		}
		return ret;
	}

	std::variant<cconf_value, std::vector<cconf_value>>
	parse_value(const std::variant<std::string, std::vector<std::string>>& value)
	{
		cconf_type whatever;
		if (std::holds_alternative<std::string>(value))
			return parse_scalar(std::get<0>(value), whatever);
		else
			return parse_array(std::get<1>(value));
	}

	void print_value(cconf_value val)
	{
		if (std::holds_alternative<std::string>(val))
		{
			std::cout << "str:" << std::get<std::string>(val);
		}
		else if (std::holds_alternative<cconf_int>(val))
		{
			std::cout << "int:" << std::get<cconf_int>(val);
		}
		else if (std::holds_alternative<cconf_float>(val))
		{
			std::cout << "float:" << std::get<cconf_float>(val);
		}
		else if (std::holds_alternative<bool>(val))
		{
			std::cout << "bool:" << (std::get<bool>(val) ? "true" : "false");
		}
	}

	std::string name;

	public:
		cconf_section sections;
	    cconf() = default;

	    cconf(std::string filename): name(filename)
		{
			std::ifstream file(filename);

			if(!file.is_open())
				throw std::runtime_error("File not found: " + filename);

		    cconf_tokens tokens = tokenize(file);
			parse_tokens(tokens);
		}

		void print()
		{
			for(auto [name, kv] : sections)
			{
				std::cout << "[" << name << "]" << std::endl;
				for(auto [key, value] : kv)
				{
					std::cout << key << " := ";
					if(std::holds_alternative<std::vector<cconf_value>>(value))
					{
						for(auto val : std::get<std::vector<cconf_value>>(value))
						{
							print_value(val);
							std::cout << " ";
						}
						std::cout << std::endl;
					} else {
						print_value(std::get<cconf_value>(value));
						std::cout << std::endl;
					}
				}
			}
		}

	    void merge(const cconf& other) { *this = other; }

	    template <typename T>
	    auto cconf_get_or(std::string sectionname,
	                      std::string fieldname,
	                      T otherwise,
	                      bool allow_wrong_type = true)
	        -> std::enable_if_t<std::is_integral<T>::value, cconf_int>
	    {
		    if (sections.count(sectionname) && sections[sectionname].count(fieldname) &&
		        std::holds_alternative<cconf_value>(sections[sectionname][fieldname]))
		    {
			    const auto& tmp = std::get<cconf_value>(sections[sectionname][fieldname]);
			    if (std::holds_alternative<cconf_int>(tmp))
			    {
				    return std::get<cconf_int>(tmp);
			    }
			    else if (!allow_wrong_type)
			    {
				    throw std::runtime_error("Unexpected type for field " + fieldname);
			    }
		    }

		    return otherwise;
	    }

	    template <typename T>
	    auto cconf_get_or(std::string sectionname,
	                      std::string fieldname,
	                      T otherwise,
	                      bool allow_wrong_type = true)
	        -> std::enable_if_t<std::is_floating_point<T>::value, cconf_float>
	    {
		    if (sections.count(sectionname) && sections[sectionname].count(fieldname) &&
		        std::holds_alternative<cconf_value>(sections[sectionname][fieldname]))
		    {
			    const auto& tmp = std::get<cconf_value>(sections[sectionname][fieldname]);
			    if (std::holds_alternative<cconf_float>(tmp))
			    {
				    return std::get<cconf_float>(tmp);
			    }
			    else if (!allow_wrong_type)
			    {
				    throw std::runtime_error("Unexpected type for field " + fieldname);
			    }
		    }

		    return otherwise;
	    }

	    template <typename T>
	    auto cconf_get_or(std::string sectionname,
	                      std::string fieldname,
	                      T otherwise,
	                      bool allow_wrong_type = true)
	        -> std::enable_if_t<!std::is_integral<T>::value, T>
	    {
		    if (sections.count(sectionname) && sections[sectionname].count(fieldname) &&
		        std::holds_alternative<cconf_value>(sections[sectionname][fieldname]))
		    {
			    const auto& tmp = std::get<cconf_value>(sections[sectionname][fieldname]);
			    if (std::holds_alternative<T>(tmp))
			    {
				    return std::get<T>(tmp);
			    }
			    else if (!allow_wrong_type)
			    {
				    throw std::runtime_error("Unexpected type for field " + fieldname);
			    }
		    }

		    return otherwise;
	    }

	    static cconf_type get_type(const std::variant<cconf_value, std::vector<cconf_value>>& val)
		{
			if(!std::holds_alternative<cconf_value>(val))
				return cconf_type::ARRAY;

		    const auto& tmp = std::get<cconf_value>(val);

		    if(std::holds_alternative<cconf_int>(tmp))
				return cconf_type::INT;
			else if(std::holds_alternative<cconf_float>(tmp))
				return cconf_type::FLOAT;
			else if(std::holds_alternative<std::string>(tmp))
				return cconf_type::STRING;
			else
				return cconf_type::BOOL;
		}
};

#endif // CPPRPG_COMMON_CCONF_HPP
