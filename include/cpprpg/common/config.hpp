#ifndef CPPRPG_COMMON_CONFIG_HPP
#define CPPRPG_COMMON_CONFIG_HPP

#include <cpprpg/common/attribute.hpp>
#include <cpprpg/common/cconf.hpp>
#include <cpprpg/common/gameobject.hpp>
#include <cstdlib>
#include <fstream>
#include <map>
#include <string>
#include <tuple>
#include <variant>
#include <vector>

#define UNUSED(expr)  \
	do                \
	{                 \
		(void)(expr); \
	} while (0)

#ifdef _WIN32
static const std::string HOME_FOLDER = std::getenv("HOMEPATH") + std::string("/");
#define BASE_FOLDER (HOME_FOLDER + "AppData/Local/cppRPG/")
#else
static const std::string HOME_FOLDER = std::getenv("HOME") + std::string("/");
#define BASE_FOLDER (HOME_FOLDER + ".config/cppRPG/")
#endif
#define CONFIG_FOLDER (BASE_FOLDER + "config/")
#define CLASS_FOLDER (CONFIG_FOLDER + "classes/")
#define SKILL_FOLDER (CONFIG_FOLDER + "skills/")
#define RACE_FOLDER (CONFIG_FOLDER + "races/")
#define PROFESSION_FOLDER (CONFIG_FOLDER + "professions/")
#define RECIPES_FOLDER (CONFIG_FOLDER + "recipes/")
#define LEVEL_CAP 100

class Skill;
class Character;
class Item;
struct Recipe;

using skill_ptr = Skill (*)();

std::tuple<size_t, size_t> get_textpos(std::ifstream& file);

struct skilltree_node
{
	lvl_type level = 0;
	std::vector<std::variant<skill_ptr, skilltree_node*>> skills;
	std::vector<skilltree_node> next;
};

struct Area : Point
{
	size_t radius;
};

enum class WearableWeight
{
	LIGHT,
	MEDIUM,
	HEAVY
};

enum class WearableType
{
	SWORD1H,
	SWORD2H,
	STAFF2H,
	STAFF1H,
	DAGGER,
	SHIELD,
	CROSSBOW,
	BOW,
	MACE,
	AXE1H,
	AXE2H,
	HELMET,
	RING,
	SHOE,
	CAPE
};

enum class SlotType
{
	HEAD,
	NECK,
	BACK,
	SHOULDERS,
	CHEST,
	FINGER,
	LEGS,
	FEET,
	MAINH,
	SIDEH
};

inline const std::map<WearableType, std::vector<SlotType>> wearableconnect = {
    {WearableType::SWORD1H, {SlotType::MAINH, SlotType::SIDEH}},
    {WearableType::SWORD2H, {SlotType::MAINH}},
    {WearableType::HELMET, {SlotType::HEAD, SlotType::BACK}}};

bool config_folder_exists();
void copy_default_configs();
void load_default_configs();

void load_class_config(std::string name);
void load_race_config(std::string name);
void load_profession_config(std::string name);
void load_recipe_config(std::string name);
void load_skilltree_config(std::string name, std::string filename);

inline cconf class_conf;
inline cconf race_conf;
inline cconf profession_conf;
extern std::map<std::string, Skill (*)()> all_skills;
inline std::map<std::string, skilltree_node> skilltrees;
extern std::map<std::string, Recipe> all_recipes;
extern std::map<std::string, Item> all_items;

#endif // CPPRPG_COMMON_CONFIG_HPP
