#ifndef CPPRPG_COMMON_OS_HPP
#define CPPRPG_COMMON_OS_HPP
#include <string>

bool create_directories(std::string name);

#endif // CPPRPG_COMMON_OS_HPP