#ifndef CPPRPG_COMMON_SKILLTREEPARSE_HPP
#define CPPRPG_COMMON_SKILLTREEPARSE_HPP

#include <cpprpg/common/config.hpp>
#include <cpprpg/skills/skill.hpp>
#include <map>

inline std::map<std::string, Skill(*)()> all_skills =
{
	{"Hit", Skill::Hit},
	{"Bash", Skill::Bash},
	{"Defend", Skill::Defend},
	{"Taunt", Skill::Taunt},
};

inline std::vector<std::string> string_split(const std::string str, char delim)
{
	std::vector<std::string> ret(1);
	bool quoted = false;
	if(delim == '"')
	{
		std::vector<std::string> tmp;
		tmp.push_back(str);
		return tmp;
	}

	for(char c: str)
	{
		if(quoted || c != delim)
		{
			if(c == '"')
				quoted = !quoted;
			ret[ret.size()-1].push_back(c);
		} else {
			ret.emplace_back();
		}
	}

	return ret;
}

inline bool unquote(std::string& str)
{
	if(str[0] != '"' || str.back() != '"')
		return false;

	str.erase(0,1);
	str.erase(str.size()-1,str.size());
	return true;
}

inline skilltree_node* find_skilltree(const std::string str)
{
	if(skilltrees.count(str) == 0)
		return nullptr;
	else
		return &skilltrees[str];
}

inline skill_ptr find_skill(const std::string str)
{
	if(all_skills.count(str) == 0)
		return nullptr;
	else
		return all_skills[str];
}

inline std::optional<std::variant<skill_ptr,skilltree_node*>> parse_skill(const std::string str)
{
	std::string tmpstr = str;
	if(tmpstr.size() < 2)
		return std::nullopt;

	bool is_skillptr;

	if(tmpstr[0] == '@')
	{
		is_skillptr = false;
		tmpstr.erase(0,1);
	} else {
		is_skillptr = true;
	}

	if(!unquote(tmpstr))
		return std::nullopt;


	skill_ptr tmpskill;
	skilltree_node* tmpskilltree;

	if(!is_skillptr)
		tmpskilltree = find_skilltree(tmpstr);
	else
		tmpskill = find_skill(tmpstr);


	if((is_skillptr && !tmpskill) || (!is_skillptr && !tmpskilltree))
	{
		return std::nullopt;
	} else if(is_skillptr) {
		return tmpskill;
	} else {
		return tmpskilltree;
	}
}

inline std::optional<std::vector<std::optional<skilltree_node>>> parse_skilltree_line(std::ifstream& file)
{
	std::vector<std::optional<skilltree_node>> nodes;

	if(file.eof())
		return nodes;

	std::string line;
	size_t next;
	lvl_type level;

	do
	{
		if(file.eof())
			return nodes;
		getline(file, line);
	} while(line.size() == 0);

	try
	{
		level = stoi(line, &next, 0);
	} catch(...){
		auto textpos = get_textpos(file);
		std::cerr << "Line " << std::get<0>(textpos) << ": " << "Level number couldn't be parsed" << std::endl;
		return std::nullopt;
	}

	if(line[next++] != ':')
	{
		auto textpos = get_textpos(file);
		std::cerr << std::get<0>(textpos) << ":" << next-1 << " " << "Expected ':', got " << line[next-1] << std::endl;
		return std::nullopt;
	}


	auto branches = string_split(line.substr(next), '|');
	for(size_t i = 0; i < branches.size(); i++)
	{
		if(branches[i].size() == 0)
		{
			nodes.emplace_back();
			continue;
		}
		nodes.push_back(skilltree_node());
		(*nodes.back()).level = level;

		auto choices = string_split(branches[i], '~');
		for(size_t j = 0; j < choices.size(); j++)
		{
			auto tmp = parse_skill(choices[j]);
			if(!tmp)
			{
				auto textpos = get_textpos(file);
				std::cerr << "Line " << std::get<0>(textpos) << " Branch " << i << " Skill " << j << ": Error parsing " << choices[j] << std::endl;
				return std::nullopt;
			}

			(*nodes.back()).skills.push_back(*tmp);
		}

	}

	return nodes;
}

inline void print_skilltree(skilltree_node& root, int a = 0)
{
	for(auto i: root.skills)
	{
		int b = a;
		if(std::holds_alternative<skill_ptr>(i))
		{
			while(b--)
				std::cout << "     ";
			std::cout << a << " " << std::get<skill_ptr>(i)().name << std::endl;
		}
	}
	std::cout << std::endl;

	for(auto i: root.next)
	{
		print_skilltree(i, a+1);
	}
}

inline bool parse_skilltree_sub(std::vector<skilltree_node*> last, std::ifstream& skilltree_file)
{
	if(skilltree_file.eof())
		return true;;

	size_t current_parent = 0;
	size_t left = last[current_parent]->skills.size();
	auto tmp = parse_skilltree_line(skilltree_file);
	if(!tmp)
	{
		return false;
	}
	for(auto current_child: *tmp)
	{
		if(left-- == 0)
		{
			current_parent++;
			if(current_parent >= last.size())
			{
				auto textpos = get_textpos(skilltree_file);
				std::cerr << "Line " << std::get<0>(textpos) << ": " << "Too many branches" << std::endl;
				return false;
			}
			left = last[current_parent]->skills.size()-1;
		}
		if(current_child)
			last[current_parent]->next.push_back(std::move(*current_child));
	}

	std::vector<skilltree_node*> current;
	for(auto old_node: last)
	{
		for(auto& new_node: old_node->next)
		{
			current.push_back(&new_node);
		}
	}

	return parse_skilltree_sub(current, skilltree_file);
}

inline std::optional<skilltree_node> parse_skilltree(std::string filename)
{
	std::ifstream skilltree_file(filename);
	if(!skilltree_file.is_open())
	{
		std::cerr << "Couldn't open " << filename << std::endl;
		return std::nullopt;
	}

	auto tmp = parse_skilltree_line(skilltree_file);
	if(!tmp || !tmp->front())
	{
		return std::nullopt;
	} else if(tmp->size() != 1) {
		std::cerr << "Found multiple skill branches at root level" << std::endl;
		return std::nullopt;
	}
	skilltree_node root = std::move(*tmp->front());
	std::vector<skilltree_node*> ptr;
	ptr.push_back(&root);
	if(!parse_skilltree_sub(ptr, skilltree_file))
		return std::nullopt;

	return root;
}

inline skilltree_node init_skilltree(std::string filename)
{
	auto tmp = parse_skilltree(filename);
	if(!tmp)
		throw std::runtime_error("Failed to parse Warrior.skilltree");

	return *tmp;
}

#endif //CPPRPG_COMMON_SKILLTREEPARSE_HPP
