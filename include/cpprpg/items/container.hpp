#ifndef CPPRPG_ITEMS_CONTAINER_HPP
#define CPPRPG_ITEMS_CONTAINER_HPP
#include <cpprpg/common/gameobject.hpp>

class Container : public Gameobject
{
	public:
	std::vector<Item> content;
	Container(Point p) : Gameobject(p, 'V') {}
};

#endif // CPPRPG_ITEMS_CONTAINER_HPP