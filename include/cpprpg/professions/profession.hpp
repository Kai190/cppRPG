#ifndef CPPRPG_PROFESSIONS_PROFESSION_HPP
#define CPPRPG_PROFESSIONS_PROFESSION_HPP

#include <cpprpg/common/config.hpp>
#include <cpprpg/professions/recipe.hpp>
#include <set>
#include <string>

class Profession
{
	public:
	Character& character;
	Attributes attributes;
	size_t lvl;
	std::vector<Skill> use;
	std::set<Recipe> recipe_list;

	Profession(const std::string professionname, Character& character);

	// for set usage
	friend bool operator<(const Profession& lhs, const Profession& rhs)
	{
		return lhs.attributes.name < rhs.attributes.name;
	}
};

#endif // CPPRPG_PROFESSIONS_PROFESSION_HPP
