#include <cpprpg/characters/character.hpp>

attr_type Character::get_max_hp()
{
	return this->get_attributes().at(AttributeType::CONSTITUTION) * 10;
}
attr_type Character::get_max_energy()
{
	return 100;
}
