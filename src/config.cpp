#include <cpprpg/common/config.hpp>
#include <cpprpg/common/default_config.hpp>
#include <cpprpg/common/skilltreeparse.hpp>
#include <cpprpg/common/os.hpp>
#include <cpprpg/common/helper.hpp>
#include <cpprpg/items/item.hpp>
#include <cpprpg/professions/recipe.hpp>
#include <filesystem>
#include <exception>

namespace fs = std::filesystem;

std::map<std::string, Recipe> all_recipes;
std::map<std::string, Item> all_items;

bool config_folder_exists()
{
	return fs::exists(CONFIG_FOLDER);
}

std::tuple<size_t,size_t> get_textpos(std::ifstream& file)
{
	file.clear();
	auto orig = file.tellg();
	file.seekg(0);
	size_t lineno = 0;
	size_t lineoffset = 0;

	while(file.tellg() != orig)
	{
		char c = file.get();
		if(c == '\n' && file.tellg() != orig)
		{
			lineoffset = 0;
			lineno++;
		} else if(c == EOF) {
			break;
		} else {
			lineoffset++;
		}
	}

	return {lineno,lineoffset};
}

void copy_default_configs()
{
	if(!create_directories(CLASS_FOLDER))
		throw std::runtime_error("Failed to create config directories");
	if(!create_directories(RACE_FOLDER))
		throw std::runtime_error("Failed to create config directories");
	if(!create_directories(PROFESSION_FOLDER))
		throw std::runtime_error("Failed to create config directories");
	if(!create_directories(RECIPES_FOLDER))
		throw std::runtime_error("Failed to create config directories");
	if(!create_directories(SKILL_FOLDER))
		throw std::runtime_error("Failed to create config directories");

	std::ofstream outfile(CLASS_FOLDER+"baseClasses.cconf");
	outfile << default_classes;
	outfile.flush();
	outfile.close();

	outfile.open(RACE_FOLDER+"baseRaces.cconf");
	outfile << default_races;
	outfile.flush();
	outfile.close();

	outfile.open(PROFESSION_FOLDER+"baseProfessions.cconf");
	outfile << default_professions;
	outfile.flush();
	outfile.close();

	outfile.open(RECIPES_FOLDER+"baseRecipes.cconf");
	outfile << default_recipes;
	outfile.flush();
	outfile.close();

	outfile.open(SKILL_FOLDER+"Warrior.skilltree");
	outfile << warrior_sktree;
	outfile.flush();
	outfile.close();
}

void load_default_configs()
{
	load_class_config(CLASS_FOLDER+"baseClasses.cconf");
	load_race_config(RACE_FOLDER+"baseRaces.cconf");
	load_profession_config(PROFESSION_FOLDER+"baseProfessions.cconf");


	all_items.insert(std::make_pair("Flour", Item{Attributes("Flour"), 1, std::nullopt}));

	load_recipe_config(RECIPES_FOLDER+"baseRecipes.cconf");
	load_skilltree_config("Warrior",SKILL_FOLDER+"Warrior.skilltree");

}

void load_class_config(std::string name)
{

	class_conf.merge(cconf(name));
}

void load_race_config(std::string name)
{
	race_conf.merge(cconf(name));
}

void load_profession_config(std::string name)
{
	profession_conf.merge(cconf(name));
}

void load_recipe_config(std::string name)
{
	merge_maps(all_recipes, init_recipes(name));
}

void load_skilltree_config(std::string name, std::string filename)
{
	skilltrees[name] =  init_skilltree(filename);
}
