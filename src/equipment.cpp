#include <algorithm>
#include <assert.h>
#include <cpprpg/characters/character.hpp>
#include <cpprpg/characters/equipment.hpp>

bool is_two_handed(WearableType w)
{
	if (w == WearableType::AXE2H || w == WearableType::BOW || w == WearableType::CROSSBOW ||
	    w == WearableType::STAFF2H || w == WearableType::SWORD2H)
		return true;
	return false;
}

bool Equipment::test_all_equips() const
{
	for (const auto& tuple : arr)
	{
		const auto& item = std::get<std::optional<Item>>(tuple);
		if (item)
			if (!item->use_requirements_test(owner))
				return false;
	}

	return true;
}

std::pair<bool, std::array<std::optional<Item>, 2>> Equipment::equip(Item wearable, SlotType slot)
{
	assert(wearable.wearable);
	if(is_two_handed(wearable.wearable->type))
	{
		if (slot == SlotType::MAINH)
		{

			auto tmp = unequip(slot);
			auto tmp2 = unequip(SlotType::SIDEH);

			if (wearable.use_requirements_test(owner))
			{

				just_equip(wearable, slot);
				if (test_all_equips())
				{
					std::get<bool>(get_tuple(SlotType::SIDEH)) = false;
					return {true, {tmp, tmp2}};
				}
				unequip(slot);
			}
			if(tmp)
				just_equip(*tmp, slot);
			if(tmp2)
				just_equip(*tmp2, SlotType::SIDEH);
		}
	}
	else
	{
		auto tmp = unequip(slot);
		if (wearable.use_requirements_test(owner))
		{
			just_equip(wearable, slot);
			if (test_all_equips())
				return {true, {tmp, std::nullopt}};
			unequip(slot);
		}
		if(tmp)
			just_equip(*tmp, slot);
	}
	return {false, {std::nullopt}};
}

void Equipment::just_equip(Item wearable, SlotType slot)
{
	assert(get_equip(slot) == std::nullopt); // slot must be empty
	assert(wearable.wearable);               // item must be a wearable
	get_equip(slot) = wearable;
}

std::optional<Item> Equipment::unequip(SlotType slot)
{
	std::optional<Item> tmp = std::nullopt;
	std::swap(tmp, get_equip(slot));
	if(tmp && is_two_handed(tmp->wearable->type))
	{
		std::get<bool>(get_tuple(SlotType::SIDEH)) = true;
	}
	return tmp;
}

void Equipment::evaluate()
{
	for (auto& tuple : arr)
	{
		const auto& wearable = std::get<std::optional<Item>>(tuple);
		if (wearable)
		{
			lvl_type tmp = 0;
			if (wearable->lvl_requirement > owner.level)
				tmp = wearable->lvl_requirement - owner.level;
			std::get<attr_type>(tuple) = std::clamp(
			    attr_type(100 -
			              (tmp + (wearable->attr_requirements.eval_diff(owner.get_attributes())))),
			    (attr_type)1,
			    (attr_type)100);
		}
	}
}

Equipment::Equipment(Character& owner) :
 owner(owner),
 arr{std::make_tuple(std::nullopt, 100, true),
     std::make_tuple(std::nullopt, 100, true),
     std::make_tuple(std::nullopt, 100, true),
     std::make_tuple(std::nullopt, 100, true),
     std::make_tuple(std::nullopt, 100, true),
     std::make_tuple(std::nullopt, 100, true),
     std::make_tuple(std::nullopt, 100, true),
     std::make_tuple(std::nullopt, 100, true),
     std::make_tuple(std::nullopt, 100, true),
     std::make_tuple(std::nullopt, 100, true)}
{
}
