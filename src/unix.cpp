#include <cpprpg/common/os.hpp>
#include <limits.h>
#include <string.h>
#include <sys/stat.h>
#include <iostream>

bool create_directories(std::string name)
{
	const char* dir = name.c_str();
	char tmp[PATH_MAX];
	char *p = NULL;
	size_t len;
	
	if(snprintf(tmp, sizeof(tmp),"%s",dir) < 0)
		return false;
	
	len = strlen(tmp);
	if(tmp[len - 1] == '/')
		tmp[len - 1] = 0;
	for(p = tmp + 1; *p; p++)
	{
		if(*p == '/')
		{
			*p = 0;
			if(mkdir(tmp, S_IRWXU) && errno != EEXIST)
				return false;
			*p = '/';
		}
	}
	if(mkdir(tmp, S_IRWXU))
		return false;
	
	return true;
}